void main() {
  var winning_apps = [
    'FNB Banking',
    'SnapScan',
    'Live Inspect',
    'WumDrop',
    'Domestly',
    'Standard Bank Shyft',
    'Khula',
    'Naked Insurance',
    'EasyEquities',
    'Amabani'
  ];
  winning_apps.sort();

  print('The Winning App List: ${winning_apps}');
  print('The winning App of 2017: ${winning_apps[8]}.');
  print('The winning App of 2018: ${winning_apps[4]}.');
  print(
      'The total number of Winning Apps since the year 2012: ${winning_apps.length}');
}
