void main() {
  winningApp fnb = new winningApp('SnapScan', 'Fin', 'Jason', 2034);
  fnb.displayInfo();
  fnb.appNameToUpper('SnapScan');
}

class winningApp {
  String appName = '', category = '', developer = '';
  int year = 0;

  winningApp(String appName, category, developer, int year) {
    this.appName = appName;
    this.category = category;
    this.developer = developer;
    this.year = year;
  }

  displayInfo() {
    print(
        'App Name: ${this.appName}\nCategory: ${this.category}\nDeveloper: ${this.developer}\nYear Won: ${this.year}');
  }

  appNameToUpper(String appName) {
    this.appName = appName;
    print('\nApp Name to ALL-CAPS: ${appName.toUpperCase()}');
  }
}
